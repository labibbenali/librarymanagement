from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label

from UserInterface.StandardButton import BasicButton


class HeaderView(GridLayout):
    def __init__(self,controller,**kwargs):
        super(HeaderView, self).__init__(**kwargs)
        self.controller=controller
        self.cols=3
        self.rows=1
        self.size_hint=(1,.15)
        self.pos_hint={"top":1}
        self.grid2 = GridLayout(padding=15, spacing=5, cols=1, rows=2,
                                size_hint=(.15, .1))
        self.grid3 = GridLayout(padding=15, spacing=5, cols=1, rows=1,size_hint=(.15, .1))
        self.grid1 = GridLayout(padding=15, spacing=5, cols=1, rows=1,size_hint=(.7, .1))

        self.add_widget(self.grid3)
        self.add_widget(self.grid1)
        self.add_widget(self.grid2)

    def display_connexion(self):
        self.grid1.clear_widgets()
        self.grid2.clear_widgets()
        self.grid3.clear_widgets()
        self.label_main_title= Label(text="Connexion", font_size=35, pos_hint={"center_x": .5})
        self.grid1.add_widget(self.label_main_title)
        return self

    def display_main_menu(self):

        self.nom_user=Label(text="3 profils",font_size=20,bold=True,size_hint=(.1,.1))
        self.btn_deconnecte=BasicButton(text="se déconnecter",font_size=17,size_hint=(.1,.1))
        self.btn_deconnecte.bind(on_press=self.controller.update_body)
        self.grid2.add_widget(self.nom_user)
        self.grid2.add_widget(self.btn_deconnecte)
        return self

    def display_common(self):

        self.btn_home=Button(text="",background_normal="Image/home_button.png")
        self.btn_home.bind(on_press=self.controller.back_home)
        self.grid3.add_widget(self.btn_home)
        return self


class HeaderController():
    def __init__(self,screen):
        self.screen=screen
        self.header_view=HeaderView(self)


    def update_body(self,*args):
        self.screen.change_body(self.screen.login.login_view)
        self.screen.change_header(self.header_view.display_connexion())
        self.screen.login.login_view.label.text=""

    def back_home(self,*args):
        self.screen.change_body(self.screen.mainMenu.mainMenuView)
        self.header_view.label_main_title.text="Menu Principale"
        self.header_view.grid3.clear_widgets()


