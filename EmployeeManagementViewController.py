from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput

from Model.salariee import Salariees, Salariee
from UserInterface.StandardButton import BasicButton


class EmployeeManagementView(BoxLayout):
    """this class concerns display about management of employees"""
    def __init__(self,controller,**kwargs):
        super(EmployeeManagementView, self).__init__(**kwargs)
        self.name="Gestions d'employees"
        self.controller=controller
        self.screen_principale=BoxLayout(size_hint=(.5,.2),padding=35,spacing=30)
        self.liste_salariee=GridLayout(size_hint_y=None, cols=1, row_default_height=50)
        self.liste_salariee.bind(minimum_height=self.liste_salariee.setter("height"))
        self.scroll=ScrollView()
        self.scroll.add_widget(self.liste_salariee)
        self.btn_myadmin=BasicButton(text="Admin",font_size=20,size_hint=(.2,.2),pos_hint={"center_y":.5})
        self.screen_principale.add_widget(self.btn_myadmin)
        self.btn_myadmin.bind(on_press=self.controller.charger_screen_admin)
        self.btn_salariee = BasicButton(text="Salariée", font_size=20, size_hint=(.2, .2),pos_hint={"center_y":.5})
        self.btn_salariee.bind(on_press=self.controller.charger_screen_salariee)
        self.screen_principale.add_widget(self.btn_salariee)
        self.box_salariee=BoxLayout(orientation="vertical")
        self.box_btn_menu=GridLayout(cols=3,rows=1,size_hint=(.7,.25),pos_hint={"top":0.8,"center_x":.5},padding=20,spacing=20)
        self.box_champs=BoxLayout(orientation="vertical",size_hint=(1,.6),spacing=20,padding=15)
        self.box_cordonnee=BoxLayout(orientation="vertical",size_hint=(.5,.5),pos_hint={"center_x":.5}
                                     ,padding=20,spacing=10)
        self.username_admin_delete = TextInput(hint_text="nom d'utilisateur", write_tab=False, font_size=18,
                                               size_hint=(.8, .2), multiline=False, pos_hint={"center_x": .5})
        self.creat_champs_add_emplyee_manager()
        self.box_salariee.add_widget(self.box_btn_menu)
        self.box_salariee.add_widget(self.box_champs)
        ######################################################
        self.boxAdmin = BoxLayout(orientation="vertical")
        self.btn_zone = GridLayout(cols=3, rows=1, size_hint=(.7, .25), pos_hint={"top": 0.8, "center_x": .5},
                                   padding=20, spacing=20)
        self.box_body = BoxLayout(orientation="vertical", size_hint=(1, .6), spacing=20, padding=15)
        self.box_cordonneeAdmin = BoxLayout(orientation="vertical", size_hint=(.5, .5), pos_hint={"center_x": .5}
                                       , padding=20, spacing=10)
        self.boxAdmin.add_widget(self.btn_zone)
        self.boxAdmin.add_widget(self.box_body)
        self.btn_page_admin()

    def creat_btn_emplyee(self):
        btn_add_employee=BasicButton(text="Ajout de salarié",font_size=18,background_color="green",size_hint=(.15,.17))
        btn_add_employee.bind(on_press=self.controller.charge_ajout_salariee)
        self.box_btn_menu.add_widget(btn_add_employee)
        btn_delete_employee=BasicButton(text="Effacer un salarié",font_size=18,background_color="red"
                                        ,size_hint=(.15,.17))
        btn_delete_employee.bind(on_press=self.controller.charge_delete_salariee)
        self.box_btn_menu.add_widget(btn_delete_employee)
        btn_liste_salarie=BasicButton(text="liste de salarié",font_size=18,background_color="blue",size_hint=(.15,.17))
        btn_liste_salarie.bind(on_press=self.controller.charge_liste_salariee)
        self.box_btn_menu.add_widget(btn_liste_salarie)

    def creat_champs_add_emplyee_manager(self,*args):
        self.username_input=TextInput(hint_text="nom d'utilisateur", write_tab=False, font_size=18,
                                                 size_hint=(.8,.2), multiline=False, pos_hint={"center_x":.5})
        self.box_cordonnee.add_widget(self.username_input)
        self.password = TextInput(hint_text="mot de passe", write_tab=False, font_size=19,
                                           size_hint=(.8, .2), password=True, multiline=False, pos_hint={"center_x":.5})
        self.box_cordonnee.add_widget(self.password)
        self.info=Label(text="",font_size=18,size_hint=(.8,.2),pos_hint={"center_x":.5})
        self.box_cordonnee.add_widget(self.info)
        btn_creat=BasicButton(text="Crée",font_size=18,size_hint=(.25,.2),background_color="green",pos_hint={"center_x":.5})
        btn_creat.bind(on_press=self.controller.save_salariee)
        self.box_cordonnee.add_widget(btn_creat)

    def creat_champs_delete_salariee(self):
        self.username_input_for_delete = TextInput(hint_text="nom d'utilisateur", write_tab=False, font_size=18,
                                                   size_hint=(.8, .2), multiline=False, pos_hint={"center_x": .5})
        self.box_cordonnee.add_widget(self.username_input_for_delete)
        self.password_of_admin = TextInput(hint_text="mot de passe de l'admin", write_tab=False, font_size=19,
                                           size_hint=(.8, .2), password=True, multiline=False, pos_hint={"center_x": .5})
        self.box_cordonnee.add_widget(self.password_of_admin)
        self.info_label = Label(text="", font_size=18, size_hint=(.8, .2), pos_hint={"center_x": .5})
        self.box_cordonnee.add_widget(self.info_label)
        btn_effacer = BasicButton(text="Effacer", font_size=18, size_hint=(.25, .2),background_color="red",pos_hint={"center_x": .5})
        btn_effacer.bind(on_press=self.controller.delete_salariee)
        self.box_cordonnee.add_widget(btn_effacer)

    ##################################################################
    #les fonction pour les admin
    #################################################################

    def btn_page_admin(self):
        btn_add_admin = BasicButton(text="Ajout d'un admin", font_size=18, background_color="green",
                                       size_hint=(.15, .17))
        btn_add_admin.bind(on_press=self.controller.ajout_admin)
        self.btn_zone.add_widget(btn_add_admin)
        btn_delete_admin = BasicButton(text="Effacer un admin", font_size=18, background_color="red",
                                          size_hint=(.15, .17))
        btn_delete_admin.bind(on_press=self.controller.delete_admin)
        self.btn_zone.add_widget(btn_delete_admin)
        btn_liste_admin = BasicButton(text="liste de admin", font_size=18, background_color="blue",
                                        size_hint=(.15, .17))
        btn_liste_admin.bind(on_press=self.controller.charge_admin_list)
        self.btn_zone.add_widget(btn_liste_admin)

    def ajout_admin(self):
        self.username_admin=TextInput(hint_text="nom d'utilisateur", write_tab=False, font_size=18,
                                                   size_hint=(.8, .2), multiline=False, pos_hint={"center_x": .5})
        self.box_cordonneeAdmin.add_widget(self.username_admin)

        self.password_admin=TextInput(hint_text="mot de passe", write_tab=False, font_size=18,
                                                   size_hint=(.8, .2), password=True,multiline=False, pos_hint={"center_x": .5})
        self.box_cordonneeAdmin.add_widget(self.password_admin)
        self.info_add_admin=Label(text="", font_size=18, size_hint=(.8, .2), pos_hint={"center_x": .5})
        self.box_cordonneeAdmin.add_widget(self.info_add_admin)
        self.btn_verify=BasicButton(text="Crée",font_size=18, size_hint=(.25, .2),background_color="green",pos_hint={"center_x": .5})
        self.box_cordonneeAdmin.add_widget(self.btn_verify)
        self.btn_verify.bind(on_press=self.controller.save_admin)
    def delete_admin(self):
        self.box_cordonneeAdmin.add_widget(self.username_admin_delete)
        self.password_admin_delete=TextInput(hint_text="mot de passe", write_tab=False, font_size=18,password=True,
                                                   size_hint=(.8, .2), multiline=False, pos_hint={"center_x": .5})
        self.box_cordonneeAdmin.add_widget(self.password_admin_delete)
        self.info_delete_admin = Label(text="", font_size=18, size_hint=(.8, .2), pos_hint={"center_x": .5})
        self.box_cordonneeAdmin.add_widget(self.info_delete_admin)
        self.delete=BasicButton(text="Effacer",font_size=18, size_hint=(.25, .2),background_color="red",pos_hint={"center_x": .5})
        self.delete.bind(on_press=self.controller.delete_admin_from_list)
        self.box_cordonneeAdmin.add_widget(self.delete)




class EmployeeManagementController():
    """this class manages the screen concerning management of employees"""
    def __init__(self,screen):
        self.screen=screen
        self.view=EmployeeManagementView(self)
        self.salariees=Salariees()


    def charger_screen_salariee(self,*args):
        self.view.box_btn_menu.clear_widgets()
        self.view.creat_btn_emplyee()
        self.view.box_cordonnee.clear_widgets()
        self.view.liste_salariee.clear_widgets()
        self.view.box_champs.clear_widgets()
        self.charger_screen(self.view.box_salariee)

    def charger_screen_admin(self,*args):
        self.charger_screen(self.view.boxAdmin)

    def charger_screen(self,vue,*args):
        self.screen.change_body(vue)

    def charge_ajout_salariee(self,*args):
        self.view.box_champs.clear_widgets()
        self.view.box_cordonnee.clear_widgets()
        self.view.creat_champs_add_emplyee_manager()
        self.view.box_champs.add_widget(self.view.box_cordonnee)

    def charge_delete_salariee(self,*args):
        self.view.box_champs.clear_widgets()
        self.view.box_cordonnee.clear_widgets()
        self.view.creat_champs_delete_salariee()
        self.view.box_champs.add_widget(self.view.box_cordonnee)


    def save_salariee(self,*args):
        if len(self.view.username_input.text)<4 or len(self.view.password.text)<4:
            self.view.info.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            salariee=Salariee(self.view.username_input.text, self.view.password.text)
            if salariee.add_salariee(self.view.username_input.text)==False:
                self.view.info.text = "Ce nom d'utilisateur existe deja , choisissez un autre"
            else:
                self.view.info.text="Done"
                self.view.username_input.text= ""
                self.view.password.text= ""

    def delete_salariee(self,*args):
        if len(self.view.username_input_for_delete.text)<4 or len(self.view.password_of_admin.text)<4:
            self.view.info_label.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            if self.salariees.delete_salariee(self.view.username_input_for_delete.text, self.view.password_of_admin.text)==False:
                self.view.info_label.text = "Ce salariee n'existe pas ou votre password incorrect"
            else:
                self.view.info_label.text = "Done"
                self.view.username_input_for_delete.text=""
                self.view.password_of_admin.text=""

    def charge_liste_salariee(self,*args):
        self.view.box_body.clear_widgets()
        self.view.box_champs.clear_widgets()
        self.view.liste_salariee.clear_widgets()
        self.view.box_champs.add_widget(self.view.scroll)
        self.salariees.charger_salariee()
        if len(self.salariees.liste_salariee)==1:
            self.view.box_champs.add_widget(Label(text="Vous avez pas de salarié",font_size=18))
        else:
            for i in range(1,len(self.salariees.liste_salariee)):
                btn=BasicButton(text=self.salariees.liste_salariee[i].username,font_size=18)
                self.view.liste_salariee.add_widget(btn)



    def ajout_admin(self,*args):
        self.view.box_body.clear_widgets()
        self.view.box_cordonneeAdmin.clear_widgets()
        self.view.ajout_admin()
        self.view.box_body.add_widget(self.view.box_cordonneeAdmin)

    def delete_admin(self,*args):
        self.view.box_body.clear_widgets()
        self.view.box_cordonneeAdmin.clear_widgets()
        self.view.delete_admin()
        self.view.box_body.add_widget(self.view.box_cordonneeAdmin)

    def save_admin(self,*args):
        if len(self.view.username_admin.text)<4 or len(self.view.password_admin.text)<4:
            self.view.info_add_admin.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            salariee = Salariee(self.view.username_admin.text, self.view.password_admin.text)
            if salariee.add_admin(self.view.username_admin.text)==False:
                self.view.info_add_admin.text = "Ce nom d'utilisateur existe deja , choisissez un autre"
            else:
                self.view.info_add_admin.text="Done"
                self.view.username_admin.text=""
                self.view.password_admin.text=""

    def delete_admin_from_list(self,*args):
        if len(self.view.username_admin_delete.text)<4 or len(self.view.password_admin_delete.text)<4:
            self.view.info_delete_admin.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            if self.salariees.delete_admin(self.view.username_admin_delete.text, self.view.password_admin_delete.text)==False:
                self.view.info_delete_admin.text = "Ce salariee n'existe pas ou votre password incorrect"
            else:
                self.view.info_delete_admin.text = "Done"
                self.view.username_admin_delete.text=""
                self.view.password_admin_delete.text=""

    def charge_admin_list(self,*args):
        self.view.box_champs.clear_widgets()
        self.view.box_body.clear_widgets()
        self.view.liste_salariee.clear_widgets()
        self.view.box_body.add_widget(self.view.scroll)
        self.salariees.charger_admin()
        if len(self.salariees.liste_admin) == 1:
            self.view.liste_salariee.add_widget(Label(text="Vous avez pas de salarié", font_size=18))
        else:
            for i in range(1, len(self.salariees.liste_admin)):
                btn = BasicButton(text=self.salariees.liste_admin[i].username, font_size=18)
                self.view.liste_salariee.add_widget(btn)











