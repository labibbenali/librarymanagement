import copy
import datetime
import json
import random
import string
import sys

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivymd.uix.picker import MDDatePicker

from Model.book import Books, Book
from Model.client import Clients, Client
from UserInterface.StandardButton import BasicButton


class BookDetailsView(GridLayout):
    def __init__(self,controller,**kwargs):
        super().__init__(**kwargs)
        self.controller = controller
        self.cols=2
        self.rows=1
        self.liste_book_emprunter = GridLayout(size_hint_y=None, cols=1, row_default_height=50)
        self.liste_book_emprunter.bind(minimum_height=self.liste_book_emprunter.setter("height"))

        self.grid1=BoxLayout(orientation="vertical",spacing=10,padding=5)
        self.grid_globe=GridLayout(cols=1,rows=2)
        self.grid2=GridLayout(cols=1,rows=2,size_hint=(1,1))
        
        self.search = TextInput(hint_text="chercher un livre", font_size=18, size_hint=(1, .16))
        self.grid2.add_widget(self.search)
        self.search.bind(text=self.controller.search_book)
        
        self.grid3=BoxLayout(size_hint=(1,.2),padding=5,spacing=10)
        self.title=Label(text="Nom de Livre",font_size=20)
        self.grid1.add_widget(self.title)
        self.category=Label(text="Categorie de livre",font_size=20)
        self.grid1.add_widget(self.category)
        self.book_place=Label(text="Emplacement du livre",font_size=20)
        self.grid1.add_widget(self.book_place)
        self.buy_price=Label(text="Prix de vente de livre",font_size=20)
        self.grid1.add_widget(self.buy_price)
        self.historique=BasicButton(text="Historique",font_size=20,size_hint=(.5,.8),pos_hint={"center_x":.5})
        self.historique.bind(on_press=self.pop_historique)
        self.grid1.add_widget(self.historique)

        self.add_widget(self.grid1)
        self.liste_book=GridLayout(size_hint_y=None,cols=1,row_default_height=50)
        self.liste_book.bind(minimum_height=self.liste_book.setter("height"))
        self.grid_details_scroll=ScrollView()
        self.grid_details_scroll.add_widget(self.liste_book)

        self.grid2.add_widget(self.grid_details_scroll)
        self.borrow_a_book_button=BasicButton(text="Emprunter",font_size=20,size_hint=(1,1))
        self.borrow_a_book_button.bind(on_press=self.emprunter_livre)
        self.grid3.add_widget(self.borrow_a_book_button)
        self.vendre_livre=BasicButton(text="Vendre",font_size=20,size_hint=(1,1))
        self.vendre_livre.bind(on_press=self.acheter_livre)
        self.add_btn=BasicButton(text="Ajout",font_size=20,size_hint=(1,1))
        self.add_btn.bind(on_press=self.popfunction)
        self.grid3.add_widget(self.vendre_livre)
        self.grid3.add_widget(self.add_btn)

        self.grid_globe.add_widget(self.grid2)
        self.grid_globe.add_widget(self.grid3)
        self.add_widget(self.grid_globe)

    def popfunction(self,*args):
        self.box = BoxLayout(orientation="vertical",size_hint=(.8,.9),padding=20,spacing=15)
        self.nom = TextInput(hint_text="nom de livre",font_size=17,size_hint=(.8,.2),pos_hint={"center_x":.5},write_tab=False,multiline=False)
        self.box.add_widget(self.nom)
        ######################################## dropdown categorie liste
        self.dropdown_categorie=DropDown()
        categorie=["Politique","Horreur","Sport","Manga","Fiction","Enfants","Histoire"]
        for i in categorie:
            btn = BasicButton(text=i, size_hint_y=None, bold=True, font_size=17)
            btn.bind(on_release=self.get_pressbutton_categorie)
            self.dropdown_categorie.add_widget(btn)
        self.main_button_categori=BasicButton(text="choisir la catégorie",size_hint=(.8,.2),pos_hint={"center_x":.5},font_size=17)
        self.main_button_categori.bind(on_release=self.dropdown_categorie.open)
        self.dropdown_categorie.bind(on_select=lambda instance, x: setattr(self.main_button_categori, "text", x))
        self.box.add_widget(self.main_button_categori)

        self.prix=TextInput(hint_text="prix de vente",font_size=17,size_hint=(.8,.2),pos_hint={"center_x":.5}
                            ,write_tab=False,multiline=False,input_filter="int")
        self.box.add_widget(self.prix)
        self.dropdown_emplacement=DropDown()
        alphabet=list(string.ascii_uppercase)
        for j in alphabet[:10]:
            for i in range(1,11):
                btn=BasicButton(text="%s-%d"%(j,i),size_hint_y=None,bold=True,font_size=17)
                btn.bind(on_release=self.get_pressedbutton_emplacement)
                self.dropdown_emplacement.add_widget(btn)
        self.main_button=BasicButton(text="choisir l'emplacement",size_hint=(.8,.2),pos_hint={"center_x":.5},font_size=17)
        self.main_button.bind(on_release=self.dropdown_emplacement.open)
        self.dropdown_emplacement.bind(on_select=lambda instance,x:setattr(self.main_button,"text",x))
        self.box.add_widget(self.main_button)
        self.lab=Label(text="",font_size=17,size_hint=(.8,.2),pos_hint={"center_x":.5})
        self.box.add_widget(self.lab)
        self.btn=BasicButton(text="OK",font_size=19,size_hint=(.15,.15),color="green",pos_hint={"center_x":.5},bold=True)
        self.box.add_widget(self.btn)
        self.pop=Popup(title="Ajout de livre",content=self.box,title_align="center",size_hint=(.7,.7))
        self.btn.bind(on_press=self.verify_input_book)
        self.pop.open()
    def verify_input_book(self,*args):
        if self.nom.text!="" and self.main_button_categori.text!="choisir la catégorie" and self.prix.text!=""and self.main_button.text!="choisir l'emplacement":
            self.controller.add_book_to()
            ###############################
            #ajout la func d'historique ici
            ###############################
            self.pop.dismiss()
        else:
            self.lab.text="il faut remplir tous les champs"

    def get_pressedbutton_emplacement(self,instance): #func to get the button pressed in dropdown
        self.dropdown_emplacement.select(instance.text)
        return instance.text

    def get_pressbutton_categorie(self,instance):
        self.dropdown_categorie.select(instance.text)
        return instance.text

    def acheter_livre(self,*args):
        self.box_acheter=BoxLayout(orientation="vertical",size_hint=(1,.8),padding=20,spacing=15)
        self.name_book=TextInput(hint_text="nom de livre à acheter",font_size=17,size_hint=(.8,.1),pos_hint={"center_x":.5}
                                 ,write_tab=False,multiline=False)
        self.box_acheter.add_widget(self.name_book)
        self.lab_book = Label(text="", font_size=17, size_hint=(.8, .15), pos_hint={"center_x": .5})
        self.box_acheter.add_widget(self.lab_book)
        self.btn_acheter=BasicButton(text="ok",font_size=19,size_hint=(.2,.1),color="green",pos_hint={"center_x":.5},bold=True)
        self.box_acheter.add_widget(self.btn_acheter)
        self.pop_acheter=Popup(title="Acheter un livre",content=self.box_acheter,title_align="center",size_hint=(.5,.5))
        self.btn_acheter.bind(on_press=self.controller.acheter_livre)
        self.pop_acheter.open()

    def emprunter_livre(self, *args):
      try:
          self.pop_emprunter.open()
      except AttributeError:
        self.scroll_emprunter = ScrollView()
        self.scroll_emprunter.clear_widgets()
        self.scroll_emprunter.add_widget(self.liste_book_emprunter)
        self.grid_emprunter = GridLayout(cols=2, rows=1)
        self.box_emprunter =BoxLayout(orientation="vertical", size_hint=(1, .8), padding=20, spacing=15)
        self.name_book_emprunter = TextInput(hint_text="nom de livre à acheter", font_size=17, size_hint=(.8, .1),
                                             pos_hint={"center_x": .5},multiline=False
                                             , write_tab=False)
        self.box_emprunter.add_widget(self.name_book_emprunter)
        self.lab_book_emprunter = Label(text="", font_size=17, size_hint=(.8, .15), pos_hint={"center_x": .5})
        self.box_emprunter.add_widget(self.lab_book_emprunter)
        self.btn_emprunter = BasicButton(text="Emprunt", font_size=19, size_hint=(.25, .1), background_color="green",
                                    pos_hint={"center_x": .5},
                                    bold=True)
        self.btn_quit=BasicButton(text="Quitter",font_size=19,size_hint=(.25,.1),background_color="red",pos_hint={"center_x": .5},
                                    bold=True)
        self.box_emprunter.add_widget(self.btn_emprunter)
        self.box_emprunter.add_widget(self.btn_quit)
        self.grid_emprunter.add_widget(self.box_emprunter)
        self.grid_emprunter.add_widget(self.scroll_emprunter)

        self.pop_emprunter = Popup(title="Ajout de livre", content=self.grid_emprunter, title_align="center", size_hint=(.7, .7))

        self.btn_emprunter.bind(on_press=self.controller.emprunter_livre1)
        self.btn_quit.bind(on_press=self.pop_emprunter.dismiss)

        self.pop_emprunter.open()

    def pop_client_emprunt(self):
        self.box_client = BoxLayout(orientation="vertical", size_hint=(1, 1), padding=20, spacing=15)
        self.nom_client = TextInput(hint_text="nom de client", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5},
                             write_tab=False,multiline=False)
        self.box_client.add_widget(self.nom_client)
        self.prenom_client = TextInput(hint_text="prenom", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5},
                                   write_tab=False,multiline=False)
        self.box_client.add_widget(self.prenom_client)
        self.adress_client = TextInput(hint_text="adress", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5},
                              write_tab=False,multiline=False)
        self.box_client.add_widget(self.adress_client)
        self.phone_client = TextInput(hint_text="numero de téléphone", font_size=17, size_hint=(.8, .2),
                                     pos_hint={"center_x": .5}, write_tab=False,input_filter="int",multiline=False)
        self.box_client.add_widget(self.phone_client)
        ############################
        #ajout de calendrier
        self.grid_calendrier=GridLayout(cols=2,rows=1,size_hint=(.8,.2),pos_hint={"center_x":.5})
        self.date_zone=TextInput(hint_text="date de retour de livre",disabled=True,font_size=18)
        self.grid_calendrier.add_widget(self.date_zone)
        calender=Button(text="",background_normal="Image/calender.png",size_hint=(.3,1))
        calender.bind(on_press=self.show_date_picker)
        self.grid_calendrier.add_widget(calender)
        self.box_client.add_widget(self.grid_calendrier)


        self.lab_client = Label(text="", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5})
        self.box_client.add_widget(self.lab_client)
        self.btn_client = BasicButton(text="OK", font_size=19, size_hint=(.15, .15), color="green", pos_hint={"center_x": .5},
                          bold=True)
        self.box_client.add_widget(self.btn_client)
        self.pop_client = Popup(title="Client", content=self.box_client, title_align="center", size_hint=(1,1))
        self.btn_client.bind(on_press=self.controller.save_user)
        self.pop_client.open()

    def show_date_picker(self,*args):
        date_dialog=MDDatePicker(min_date=datetime.date.today(),max_date=datetime.date(2500, 1, 1))
        date_dialog.bind(on_save=self.save_date)
        date_dialog.open()
    def save_date(self,value,date,*args):
        self.date_zone.text=str(date)

    def pop_historique(self,*args):
        self.box_historique=BoxLayout(orientation="vertical",size_hint=(1, 1), padding=20, spacing=15)
        self.bar_btn=GridLayout(cols=3,rows=1,size_hint=(1,.2),spacing=10)
        self.btn_emprunt=BasicButton(text="Historique d'emprunt",font_size=18,background_color="blue")
        self.btn_emprunt.bind(on_press=self.charge_historique_emprunt)

        self.bar_btn.add_widget(self.btn_emprunt)
        self.btn_vendre=BasicButton(text="Historique de vendre",font_size=18,background_color="red")
        self.btn_vendre.bind(on_press=self.charge_historique_vendre)

        self.bar_btn.add_widget(self.btn_vendre)
        self.btn_ajout=BasicButton(text="Historique d'ajout",font_size=18,background_color="green")
        self.btn_ajout.bind(on_press=self.charge_historique_ajout)
        self.bar_btn.add_widget(self.btn_ajout)

        self.box_historique.add_widget(self.bar_btn)


        self.liste_historique = GridLayout(size_hint_y=None, cols=1, row_default_height=50,padding=20,spacing=20)
        self.liste_historique.bind(minimum_height=self.liste_historique.setter("height"))

        self.scroll_historique=ScrollView()
        self.scroll_historique.add_widget(self.liste_historique)
        self.box_historique.add_widget(self.scroll_historique)
        #add l'historique de l'ajout ds livres
        ##############################################
        self.pop_hist=Popup(title="Historique",content=self.box_historique,title_align="center", size_hint=(.7,.7))
        self.pop_hist.open()

    def charge_historique_ajout(self,*args):
        self.liste_historique.clear_widgets()
        self.controller.charger_add_book_historique()
    def charge_historique_vendre(self,*args):
        self.liste_historique.clear_widgets()
        self.controller.charger_delete_book_historique()
    def charge_historique_emprunt(self,*args):
        self.liste_historique.clear_widgets()
        self.controller.charger_emprunt_book_historique()


class BookDetailsController():
    def __init__(self,screen):
        self.view = BookDetailsView(self)
        self.screen = screen
        self.livres = Books()
        self.livres.charger_livre()
        self.livres.charger_livre_historique()
        self.livres.charger_emprunt()
        self.charger_book_liste()
        self.charger_liste_emprunt()
        self.book = 0

    def charger_book_liste(self):
        for i in range(1, len(self.livres.liste_livre)):
            btn = BasicButton(id=self.livres.liste_livre[i], text=self.livres.liste_livre[i].nom, font_size=20)
            btn.bind(on_press=self.update_detail)
            self.view.liste_book.add_widget(btn)

    def update_detail(self, instance):
        self.view.title.text = "nom : " + instance.id.nom
        self.view.category.text = "categorie : " + instance.id.categorie
        self.view.buy_price.text = "prix de vente : " + instance.id.prix + " €"
        self.view.book_place.text = "emplacement : " + instance.id.emplacement

    def add_book_to(self):
        self.un_livre = Book(self.view.nom.text, self.view.main_button_categori.text, self.view.prix.text,
                             datetime.datetime.now().strftime("%Y-%m-%d %H:%M"), self.view.main_button.text)
        self.un_livre.add_book()
        self.livres.charger_livre()
        self.view.liste_book.clear_widgets()
        self.charger_book_liste()

    def acheter_livre(self, *args):
        if self.livres.delete_book(self.view.name_book.text) == False:
            self.view.lab_book.text = "Ce livre n'existe pas "
        else:
            self.livres.charger_livre()
            self.view.liste_book.clear_widgets()
            self.charger_book_liste()
            self.view.pop_acheter.dismiss()
#chargement la liste des livre emprunté
    def charger_liste_emprunt(self):
        self.clients = Clients()
        self.clients.charger_client()
        self.livres.charger_emprunt()
        for i in range(1, len(self.livres.liste_emprunt)):
            btn = BasicButton(id=self.clients.liste_client[i], text=self.livres.liste_emprunt[i].nom,
                               font_size=20)
            btn.bind(on_press=self.pop_liste_client_book)
            self.view.liste_book_emprunter.add_widget(btn)
    def pop_liste_client_book(self,instance):
        self.box_liste_client_book=BoxLayout(orientation="vertical", size_hint=(1, 1), padding=20, spacing=15)
        self.box_btn_emprunt = BoxLayout(size_hint=(1, .2), pos_hint={"center_x": .5},spacing=10)

        self.lab_client_detail = Label(text="Détail Client", font_size=22, size_hint=(.2, .1), color="green",
                                     pos_hint={"center_x": .5}, bold=True)
        self.box_liste_client_book.add_widget(self.lab_client_detail)
        self.name_client = Label(text="nom : "+instance.id.nom, font_size=17, size_hint=(.2, .1),pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.name_client)

        self.client_prenom = Label(text="prenom : "+instance.id.prenom, font_size=17, size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.client_prenom)
        self.client_adress = Label(text="adress : "+instance.id.adress, font_size=17, size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.client_adress)

        self.client_phone = Label(text="phone : "+instance.id.phone_number, font_size=19, size_hint=(.2, .1),pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.client_phone)

        self.date_emprunt=Label(text="date d'emprunt : "+instance.id.date_emprunter,font_size=19, size_hint=(.2, .1),pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.date_emprunt)

        self.date_retour= Label(text="date de retour : " + instance.id.date_retour, font_size=19,
                                  size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.date_retour)

        self.lab_book_detail=Label(text="Détail Livre", font_size=22, size_hint=(.2, .1), color="green",
                                   pos_hint={"center_x": .5}, bold=True)
        self.box_liste_client_book.add_widget(self.lab_book_detail)

        self.book_client=Label(text="nom de livre : "+instance.id.book["nom"], font_size=19, size_hint=(.2, .1),pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.book_client)

        self.categorie_book_client = Label(text="categorie : " + instance.id.book["categorie"], font_size=19, size_hint=(.2, .1),
                                 pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.categorie_book_client)

        self.prix_client_book = Label(text="prix : " + instance.id.book["prix"], font_size=19,
                                           size_hint=(.2, .1),pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.prix_client_book)

        self.emplacement_book_client = Label(text="Emplacement : " + instance.id.book["Emplacement"], font_size=19,
                                      size_hint=(.2, .1),pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.emplacement_book_client)
        self.box_liste_client_book.add_widget(self.box_btn_emprunt)
        self.btn_rendu=BasicButton(id=instance.id,text="Rendu",font_size=18,background_color="green",size_hint=(.22,.3),pos_hint={"center_x":.5})
        self.btn_rendu.bind(on_press=self.delete_emprunt_book)
        self.box_liste_client_book.add_widget(self.btn_rendu)
        self.pop_client = Popup(title="Detail livre  emprunt", content=self.box_liste_client_book, title_align="center",
                                 size_hint=(.8, .8))
        self.pop_client.open()
    def delete_emprunt_book(self,instance):
        x=""
        with open("Data/client.json", "r") as f:
            data=json.load(f)
        data2=copy.deepcopy(data)
        for i,j in data.items():
            if instance.id.nom==data[i]["nom"] and instance.id.book["nom"]==data[i]["book"]["nom"] :
                data2.pop(i,None)
        with open("Data/client.json", "w") as f:
            json.dump(data2,f,indent=2)
        with open("Data/emprunt.json", "r") as f:
            data=json.load(f)
        data1=copy.deepcopy(data)

        for i, j in data.items():
            if instance.id.book["nom"]==data[i]["nom"] and instance.id.book["categorie"]==data[i]["categorie"]:
                x=data1.pop(i,None)
        with open("Data/emprunt.json", "w") as f:
            json.dump(data1,f,indent=2)
        with open("Data/book.json", "r") as f:
            donnee=json.load(f)
        donnee[random.choice(range(sys.maxsize))]=x
        with open("Data/book.json", "w") as f:
            json.dump(donnee,f,indent=2)
        self.livres.charger_livre()
        self.view.liste_book.clear_widgets()
        self.charger_book_liste()
        self.view.liste_book_emprunter.clear_widgets()
        self.charger_liste_emprunt()
        self.pop_client.dismiss()
            ##############


    def emprunter_livre1(self, *args):
        if self.livres.emprunter_book(self.view.name_book_emprunter.text) == False:
            self.view.lab_book_emprunter.text = "ce livre n'existe pas"
        else:
            self.view.pop_client_emprunt()


    def save_user(self,*args):
        if len(self.view.nom_client.text)<3 or len(self.view.prenom_client.text)<3 or len(self.view.adress_client.text)<3 or \
              len(self.view.phone_client.text)<10 :
            self.view.lab_client.text="Vous avez oublié des remplir des champs ou vos données sont erronés"
        else:
            self.add_client()
            self.livres.charger_livre()
            self.view.liste_book.clear_widgets()
            self.charger_book_liste()

            self.livres.charger_emprunt()
            self.view.liste_book_emprunter.clear_widgets()

            self.charger_liste_emprunt()
            self.view.name_book_emprunter.text = ""
            self.view.lab_book_emprunter.text = ""
            self.view.nom_client.text=""
            self.view.prenom_client.text=""
            self.view.adress_client.text=""
            self.view.phone_client.text=""
            self.view.pop_client.dismiss()

    def add_client(self):
        self.un_client=Client(self.view.nom_client.text,self.view.prenom_client.text,self.view.adress_client.text,
                              self.view.phone_client.text,self.clients.emprunter_client(self.view.name_book_emprunter.text)
                              ,str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")),self.view.date_zone.text)
        self.un_client.charge_client()

    # fonctions pour les comportement de bouton historique
    def charger_add_book_historique(self):
        self.view.liste_historique.clear_widgets()
        self.livres.charger_livre_historique()
        for i in range(1, len(self.livres.liste_livre_historique)):
            btn = BasicButton(id=self.livres.liste_livre_historique[i],text="Livre : "+self.livres.liste_livre_historique[i].nom+" ajouté le : "+self.livres.liste_livre_historique[i].date_ajout
                               , font_size=17,background_color="green")
            btn.bind(on_press=self.pop_detail_historique_book_ajout)
            self.view.liste_historique.add_widget(btn)

    def charger_delete_book_historique(self):
        self.view.liste_historique.clear_widgets()
        self.livres.charger_livre_delete_historique()
        for i in range(1, len(self.livres.liste_livre_delete)):
            btn = BasicButton(id=self.livres.liste_livre_delete[i],text="Livre : "+self.livres.liste_livre_delete[i].nom+" vendu le : "+self.livres.liste_livre_delete[i].date_supression
                               , font_size=17,background_color="red")
            btn.bind(on_press=self.pop_detail_historique_book_delete)
            self.view.liste_historique.add_widget(btn)

    def charger_emprunt_book_historique(self):
        self.view.liste_historique.clear_widgets()
        self.clients.charger_client_historique()
        for i in range(1,len(self.clients.liste_client_historique)):
            btn=BasicButton(id=self.clients.liste_client_historique[i],text="Livre : "+self.clients.liste_client_historique[i].book["nom"]+
                                  " emprunté par :"+self.clients.liste_client_historique[i].nom+" le : "+
                                  self.clients.liste_client_historique[i].date_emprunter,
                             font_size=17,background_color="blue")


            btn.bind(on_press=self.pop_emprunt_historique)
            self.view.liste_historique.add_widget(btn)

   #modele  pour les fonctions des actions des bouton effacer selon chaque historique
    def model_delete_historique(self,instance):
        self.box_liste_book = BoxLayout(orientation="vertical", size_hint=(1, 1), padding=20, spacing=15)
        self.lab_book_detail = Label(text="Détail Livre", font_size=25, size_hint=(.2, .1), color="green",
                                     pos_hint={"center_x": .5}, bold=True)
        self.box_liste_book.add_widget(self.lab_book_detail)

        self.book_name= Label(text="nom de livre : " + instance.id.nom, font_size=19, size_hint=(.2, .1),
                                 pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.book_name)

        self.categorie_book= Label(text="categorie : " + instance.id.categorie, font_size=19,
                                           size_hint=(.2, .1),
                                           pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.categorie_book)

        self.prix_book= Label(text="prix : " + instance.id.prix, font_size=19,
                                      size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.prix_book)

        self.emplacement_book = Label(text="Emplacement : "+instance.id.emplacement, font_size=19,
                                             size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.emplacement_book)
        self.date_ajout= Label(text="Date d'ajout : " + instance.id.date_ajout, font_size=19,
                                     size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.date_ajout)
        self.date_vendre=Label(text="", font_size=19,size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.date_vendre)

        self.box_btn=BoxLayout(size_hint=(.8,.2),pos_hint={"center_x":.5},spacing=10)
        self.effacer_ajout = BasicButton(id=instance.id, text="Effacer", font_size=17, background_color="red")
        self.box_btn.add_widget(self.effacer_ajout)

        self.btn_cancel=BasicButton(text="Annuler",font_size=17)
        self.box_btn.add_widget(self.btn_cancel)

        self.box_liste_book.add_widget(self.box_btn)
        self.pop_client = Popup(title="Detail livre  emprunt", content=self.box_liste_book, title_align="center",
                                size_hint=(.5, .5))
        self.btn_cancel.bind(on_press=self.pop_client.dismiss)
        self.pop_client.open()

    def pop_detail_historique_book_ajout(self,instance):
        self.model_delete_historique(instance)
        self.effacer_ajout.bind(on_press=self.delete_historique_ajout)

  # fonction pour le bouton effacer l'historique de vendre
    def pop_detail_historique_book_delete(self,instance):
        self.model_delete_historique(instance)
        self.effacer_ajout.bind(on_press=self.delete_historique_vendre)
        self.date_vendre.text="Date de vendre : " + instance.id.date_supression

    def pop_emprunt_historique(self,instance):
        self.pop_liste_client_book(instance)
        self.box_liste_client_book.remove_widget(self.btn_rendu)
        self.effacer_emprunt = BasicButton(id=instance.id,text="Effacer", font_size=17, background_color="red")
        self.btn_cancel_emprunt = BasicButton(text="Annuler", font_size=17)
        self.btn_cancel_emprunt.bind(on_press=self.pop_client.dismiss)
        self.effacer_emprunt.bind(on_press=self.delete_historique_emprunt)
        self.box_btn_emprunt.add_widget(self.effacer_emprunt)
        self.box_btn_emprunt.add_widget(self.btn_cancel_emprunt)

    def delete_historique_ajout(self,instance):
        with open("Data/book_historique.json", "r") as f:
            data=json.load(f)
        data2=copy.deepcopy(data)
        for i,j in data.items():
            if instance.id.nom==data[i]["nom"] and instance.id.prix==data[i]["prix"] :
                data2.pop(i,None)
        with open("Data/book_historique.json", "w") as f:
            json.dump(data2,f,indent=2)
        self.charger_add_book_historique()
        self.pop_client.dismiss()

    def delete_historique_vendre(self,instance):
        with open("Data/delete_book_historique.json", "r") as f:
            data=json.load(f)
        data2=copy.deepcopy(data)
        for i,j in data.items():
            if instance.id.nom==data[i]["nom"] and instance.id.prix==data[i]["prix"] :
                data2.pop(i,None)
        with open("Data/delete_book_historique.json", "w") as f:
            json.dump(data2,f,indent=2)

        self.charger_delete_book_historique()
        self.pop_client.dismiss()

    def delete_historique_emprunt(self,instance):
        with open("Data/emprunt_historique.json", "r") as f:
            data=json.load(f)
        data2=copy.deepcopy(data)
        for i,j in data.items():
            if instance.id.nom==data[i]["nom"] and instance.id.book["nom"]==data[i]["book"]["nom"] :
                data2.pop(i,None)
        with open("Data/emprunt_historique.json", "w") as f:
            json.dump(data2,f,indent=2)
        self.charger_emprunt_book_historique()
        self.pop_client.dismiss()
        
    
    def search_book(self,instance,*args):
        self.view.liste_book.clear_widgets()
        is_here=False
        for i in range(1,len(self.livres.liste_livre)):
            if instance.text in self.livres.liste_livre[i].nom :
                btn = BasicButton(id=self.livres.liste_livre[i], text=self.livres.liste_livre[i].nom, font_size=20)
                btn.bind(on_press=self.update_detail)
                self.view.liste_book.add_widget(btn)
                is_here=True
            elif instance.text not in self.livres.liste_livre[i].nom and is_here==False :
                self.view.liste_book.clear_widgets()
            elif instance.text=="":
                self.view.liste_book.clear_widgets()
                self.charger_book_liste()

