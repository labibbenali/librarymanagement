import json

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from UserInterface.StandardButton import BasicButton


class LoginView(BoxLayout):
    def __init__(self,controller,**kwargs):
        super(LoginView, self).__init__(**kwargs)
        self.size_hint=(.3,.4)
        self.controller=controller
        self.pos_hint={"center_x":.5}
        self.orientation="vertical"
        self.spacing=10
        self.padding=120
        self.grid_username_password=GridLayout(cols=1,rows=3,size_hint=(.6,1),pos_hint={"center_x":.5})
        self.username_login=TextInput(hint_text="write your username",font_size=22,size_hint=(.5,.55)
                                      ,write_tab=False,multiline=False)
        self.grid_username_password.add_widget(self.username_login)
        self.password_login=TextInput(hint_text="write your password",font_size=22,multiline=False,
                                      password=True,size_hint=(.5,.55),write_tab=False)
        self.grid_username_password.add_widget(self.password_login)

        self.label=Label(text="",font_size=19)
        self.grid_username_password.add_widget(self.label)
        self.add_widget(self.grid_username_password)

        self.grid_btn=GridLayout(cols=1,rows=1,size_hint=(.2,.2),pos_hint={"center_x":.5})
        self.btn_login=BasicButton(text="Log in",font_size=22,size_hint=(.1,.25))
        self.btn_login.bind(on_press=self.controller.verify_user)
        self.grid_btn.add_widget(self.btn_login)
        self.add_widget(self.grid_btn)

class LoginController():
    def __init__(self,screen):
        self.login_view=LoginView(self)
        self.screen=screen

    def verify_user(self,*args):
        with open("Data/users.json", "r") as f:
            data=json.load(f)
            for i,j in data.items():
                if j["username"]==self.login_view.username_login.text and j["password"]==self.login_view.password_login.text:
                    self.screen.change_header(self.screen.headercontrol.header_view.display_main_menu())
                    self.screen.mainMenu.mainMenuView.employees_management_button.disabled = False
                    self.screen.mainMenu.mainMenuView.employees_management_button.text = "Gestions des employees"
                    self.screen.mainMenu.mainMenuView.employees_management_button.background_color = "#3D6998"
                    self.screen.change_body(self.screen.mainMenu.mainMenuView)
                    self.screen.headercontrol.header_view.nom_user.text=j["username"]
                    self.screen.headercontrol.header_view.label_main_title.text="Menu Principale"
                    self.login_view.username_login.text=""
                    self.login_view.password_login.text=""

                else:
                    with open("Data/salariee.json", "r") as f:
                        data1 = json.load(f)
                        for i, j in data1.items():
                            if j["username"] == self.login_view.username_login.text and j[
                                "password"] == self.login_view.password_login.text:
                                self.screen.change_header(self.screen.headercontrol.header_view.display_main_menu())
                                self.screen.mainMenu.mainMenuView.employees_management_button.disabled=True
                                self.screen.mainMenu.mainMenuView.employees_management_button.text=""
                                self.screen.mainMenu.mainMenuView.employees_management_button.background_color=(0,0,0,0)
                                self.screen.change_body(self.screen.mainMenu.mainMenuView)
                                self.screen.headercontrol.header_view.nom_user.text = j["username"]
                                self.screen.headercontrol.header_view.label_main_title.text = "Menu Principale"
                                self.login_view.username_login.text = ""
                                self.login_view.password_login.text = ""
                            else:
                                    self.login_view.label.text="username or password is wrong Verify and try agin"