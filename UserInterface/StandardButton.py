from kivy.uix.button import Button


class BasicButton(Button):
    def __init__(self, id=None, background_color="#3D6998", **kwargs):
        Button.__init__(self, **kwargs)
        self.id = id
        self.background_normal = ""
        self.background_color = background_color
        self.color = "#dfe6e9"
        self.font_size = 22


class ClassicButton(Button):
    def __init__(self, text="press", **kwargs):
        Button.__init__(self, **kwargs)
        self.text = text
        self.background_normal = ""
        self.background_color = "#3D6998"
        self.color = "#dfe6e9"
        self.font_size = 22
