import copy
import datetime
import json
import random
import sys


class Client():
    def __init__(self,nom,prenom,adress,phone_number,book,date_emprunt,date_retour):
        self.nom=nom
        self.prenom=prenom
        self.adress=adress
        self.phone_number=phone_number
        self.book=book
        self.date_emprunter=date_emprunt
        self.date_retour=date_retour
    def get_data(self):
        return {
            "nom": self.nom,
            "prenom": self.prenom,
            "adress": self.adress,
            "phone":self.phone_number,
            "book": self.book,
            "Date d'emprunt": self.date_emprunter,
            "Date de retour": self.date_retour
        }

    def charge_client(self):
        with open("Data/client.json","r") as f:
            data=json.load(f)
        data[random.choice(range(sys.maxsize))]=self.get_data()
        with open("Data/client.json","w") as f:
            json.dump(data,f,indent=2)
        with open("Data/emprunt_historique.json","r") as f:
            d=json.load(f)
        d=data
        with open("Data/emprunt_historique.json","w") as f:
            json.dump(data,f,indent=2)


class Clients():
    def __init__(self):
        self.liste_client=[]
        self.liste_client_historique=[]

    def charger_client(self):
        self.liste_client=[]
        with open("Data/client.json","r") as f:
            data=json.load(f)
        for i,j in data.items():
            client=Client(j["nom"],j["prenom"],j["adress"],j["phone"],j["book"],j["Date d'emprunt"],j["Date de retour"])
            self.liste_client.append(client)

    def charger_client_historique(self):
        self.liste_client_historique=[]
        with open("Data/emprunt_historique.json","r") as f:
            d=json.load(f)
        for i,j in d.items():
            client = Client(j["nom"], j["prenom"], j["adress"], j["phone"], j["book"],j["Date d'emprunt"], j["Date de retour"])
            self.liste_client_historique.append(client)
    def emprunter_client(self,nom):
        x = 0
        is_exist = False
        with open("Data/book.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)

        for i in data:
            if nom == data[i]["nom"]:
                x = data2.pop(i, None)
                with open("Data/book.json", "w") as f:
                    json.dump(data2, f, indent=2)

                with open("Data/emprunt.json", "r") as f:
                    donnee = json.load(f)
                p=range(-sys.maxsize,0)
                donnee[random.choice(p)] = x
                with open("Data/emprunt.json", "w") as f:
                    json.dump(donnee, f, indent=2)
                return x


