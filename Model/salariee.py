import copy
import json
import random
import sys


class Salariee():
    def __init__(self, username, pasword):
        self.username = username
        self.passsword = pasword

    def get_data(self):
        return {"username": self.username,
                "password": self.passsword}

    def add_salariee(self,username):
        try:
            with open("Data/salariee.json", "r") as f:
                data = json.load(f)
            for i , j in data.items():
                if j["username"]==username:
                    return False
            data[random.choice(range(sys.maxsize))] = self.get_data()
            with open("Data/salariee.json", "w") as f:
                json.dump(data, f, indent=2)
        except:
            print("le probleme c'est ici dans la classe Salariee dans la fonction add_salariee")
        return True

    def add_admin(self,username):
        try:
            with open("Data/users.json", "r") as f:
                data = json.load(f)
            for i , j in data.items():
                if j["username"]==username:
                    return False
            data[random.choice(range(sys.maxsize))] = self.get_data()
            with open("Data/users.json", "w") as f:
                json.dump(data, f, indent=2)
        except:
            print("le probleme c'est ici dans la classe Salariee dans la fonction add_admin")
        return True


class Salariees():
    def __init__(self):
        self.liste_salariee = []
        self.liste_admin=[]

    def charger_salariee(self):
        self.liste_salariee = []
        try:
            with open("Data/salariee.json", "r") as f:
                data = json.load(f)
            for i, j in data.items():
                salariee = Salariee(j["username"], j["password"])
                self.liste_salariee.append(salariee)
        except:
            print("probleme en class Salariees , fonction charger_salariee")

    def charger_admin(self):
        self.liste_admin= []
        try:
            with open("Data/users.json", "r") as f:
                data = json.load(f)
            for i, j in data.items():
                salariee = Salariee(j["username"], j["password"])
                self.liste_admin.append(salariee)
        except:
            print("probleme en class Salariees , fonction charger_admin")




    def delete_salariee(self,username,password_admin):
        is_exist=False
        try:
            with open("Data/salariee.json","r") as f:
                data=json.load(f)
            data1=copy.deepcopy(data)
            with open("Data/users.json","r") as file:
                data2=json.load(file)
            for m, n in data2.items():
                for i, j in data.items():
                    if username == j["username"] and password_admin==n["password"]:
                        data1.pop(i,None)
                        is_exist=True

                        with open("Data/salariee.json", "w") as f:
                            json.dump(data1, f, indent=2)
        except:
            print("probleme en class salariee fonction delete_salariee")
        return is_exist

    def delete_admin(self,username,password_admin):
        is_exist=False
        try:
            with open("Data/users.json","r") as f:
                data=json.load(f)
            data1=copy.deepcopy(data)
            with open("Data/users.json","r") as file:
                data2=json.load(file)
            for m, n in data2.items():
                for i, j in data.items():
                    if username == j["username"] and password_admin==n["password"]:
                        data1.pop(i,None)
                        is_exist=True
                        pass
                        with open("Data/users.json", "w") as f:
                            json.dump(data1, f, indent=2)
        except:
            print("probleme en class salariee fonction delete_admin")
        return is_exist