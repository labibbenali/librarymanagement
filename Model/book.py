import copy
import datetime
import json
import random
import sys


class Book():
    def __init__(self, nom, categorie, prix,date_ajout,emplacement):
        self.nom = nom
        self.categorie = categorie
        self.prix = prix
        self.date_ajout=date_ajout
        self.date_supression=""
        self.emplacement = emplacement

    def get_data(self):
        return {
            "nom": self.nom,
            "categorie": self.categorie,
            "prix": self.prix,
            "Emplacement": self.emplacement,
            "Date d'ajout": self.date_ajout,
                }

    def add_book(self):
        with open("Data/book.json", "r") as f:
            data = json.load(f)
        data[random.choice(range(sys.maxsize))] = self.get_data()
        with open("Data/book.json", "w") as f:
            json.dump(data, f, indent=2)
        with open("Data/book_historique.json", "r") as f:
            data = json.load(f)
        data[len(data)] = self.get_data()
        with open("Data/book_historique.json", "w") as f:
            json.dump(data, f, indent=2)


class Books():
    def __init__(self):
        self.liste_livre = []
        self.liste_emprunt=[]
        self.liste_livre_historique=[]
        self.liste_livre_delete=[]

    def charger_livre(self):
        self.liste_livre=[]
        with open("Data/book.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            livre = Book(j["nom"], j["categorie"], j["prix"] , j["Date d'ajout"] , j["Emplacement"])
            self.liste_livre.append(livre)

    def charger_livre_historique(self):
        self.liste_livre_historique=[]
        with open("Data/book_historique.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            livre = Book(j["nom"], j["categorie"], j["prix"],j["Date d'ajout"],j["Emplacement"])
            self.liste_livre_historique.append(livre)
    def charger_livre_delete_historique(self):
        self.liste_livre_delete = []
        with open("Data/delete_book_historique.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            livre = Book(j["nom"], j["categorie"], j["prix"],j["Date d'ajout"], j["Emplacement"])
            livre.date_supression=j["date de vendre"]
            self.liste_livre_delete.append(livre)


    def charger_emprunt(self):
        self.liste_emprunt=[]
        with open("Data/emprunt.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            livre = Book(j["nom"], j["categorie"], j["prix"],j["Date d'ajout"],j["Emplacement"])
            self.liste_emprunt.append(livre)

    def delete_book(self,nom):
        x=0
        is_exist=False
        with open("Data/book.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)
        for i in data:
            if nom == data[i]["nom"]:
                x=data2.pop(i, None)
               # print("x :",x)
                is_exist = True
                with open("Data/book.json", "w") as f:
                    json.dump(data2, f, indent=2)
                with open("Data/delete_book_historique.json","r") as f:
                    d=json.load(f)
                    x["date de vendre"]=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
                p=range(-sys.maxsize,0)
                d[random.choice(p)]=x
                with open("Data/delete_book_historique.json","w") as f:
                    json.dump(d,f,indent=2)

        return is_exist

    def emprunter_book(self,nom):
        x=0
        is_exist = False
        with open("Data/book.json", "r") as f:
            data = json.load(f)
        for i in data:
            if nom == data[i]["nom"]:
                is_exist=True
        return is_exist


