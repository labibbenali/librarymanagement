from kivy.core.window import Window
from kivy.uix.gridlayout import GridLayout
from kivymd.app import MDApp

from EmployeeManagementViewController import EmployeeManagementController
from HeaderViewController import HeaderController
from booksDetailViewController import BookDetailsController
from loginViewController import LoginController
from mainMenuViewController import MainMenuViewController

Window.size=(1200, 680)

class Fenetre(GridLayout):
    def __init__(self,**kwargs):
        super(Fenetre, self).__init__(**kwargs)
        self.cols=1
        self.rows=3
        self.grid_header=GridLayout(cols=1,rows=1,size_hint=(1,.15))#,pos_hint={"top":1})
        self.grid_body=GridLayout(cols=1,rows=1,size_hint=(1,.8))

        self.add_widget(self.grid_header)
        self.add_widget(self.grid_body)

    def display_header(self,v):
        self.grid_header.clear_widgets()
        self.grid_header.add_widget(v)
    def display_body(self,view):
        self.grid_body.clear_widgets()
        self.grid_body.add_widget(view)

class Screen():
    def __init__(self):
        self.fenetre=Fenetre()
        self.headercontrol=HeaderController(self)
        self.login=LoginController(self)
        self.mainMenu=MainMenuViewController(self)
        self.bookdetail=BookDetailsController(self)
        self.employeemanagement=EmployeeManagementController(self)


        self.fenetre.display_header(self.headercontrol.header_view.display_connexion())
        self.fenetre.display_body(self.login.login_view)

    def change_header(self,view):
        self.fenetre.display_header(view)
    def change_body(self,view):
        self.fenetre.display_body(view)



class Bibliotheque(MDApp):
    def build(self):
        self.theme_cls.theme_style="Dark"
        self.sc=Screen()
        return self.sc.fenetre
Bibliotheque().run()
