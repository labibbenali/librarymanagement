from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout

from UserInterface.StandardButton import BasicButton


class MainMenuView(BoxLayout):
    def __init__(self,controller,**kwargs):
        super(MainMenuView, self).__init__(**kwargs)
        self.controller=controller
        self.orientation="vertical"
        self.size_hint=(.8,.8)
        self.pos_hint={"center_x":.5}
        self.padding = 45

        self.grid=GridLayout(cols=1,rows=2,spacing=20,size_hint=(.5,.1),pos_hint={"center_x":.5})
        self.books_management=BasicButton(text="Gestion des livres",font_size=25,size_hint=(1,.25))
        self.books_management.bind(on_press=self.controller.update_body)
        self.grid.add_widget(self.books_management)
        self.employees_management_button=BasicButton(text="Gestions des employees",font_size=25,size_hint=(1,.25))
        self.employees_management_button.bind(on_press=self.controller.update_body_emplyeemanagemnt)
        self.grid.add_widget(self.employees_management_button)
        self.add_widget(self.grid)

class MainMenuViewController():
    def __init__(self,screen):
        self.screen=screen
        self.mainMenuView=MainMenuView(self)

    def update_body(self,*args):
        self.screen.change_body(self.screen.bookdetail.view)
        self.screen.change_header(self.screen.headercontrol.header_view.display_common())
        self.screen.headercontrol.header_view.label_main_title.text = "Fiche détaillée d'un livre"
    def update_body_emplyeemanagemnt(self,*args):
        self.screen.change_body(self.screen.employeemanagement.view.screen_principale)
        self.screen.change_header(self.screen.headercontrol.header_view.display_common())
        self.screen.headercontrol.header_view.label_main_title.text="Gesion des Employees"



